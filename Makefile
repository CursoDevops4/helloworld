CC=gcc
CFLAGS=-c -pedantic -Wall
LDFLAGS=-pedantic -Wall
SOURCES=main.cpp lib.cpp
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=Program

all: $(SOURCES) $(EXECUTABLE)
	
$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	-rm  -f *.o

cleanall:
	-rm  -f *.o 
	-rm -f $(EXECUTABLE)

cstart: all start

start:
	./$(EXECUTABLE)

rebuild: cleanall all
